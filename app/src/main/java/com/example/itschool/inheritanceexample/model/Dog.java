package com.example.itschool.inheritanceexample.model;

import android.util.Log;

import com.example.itschool.inheritanceexample.R;
import com.example.itschool.inheritanceexample.model.Animal;

/**
 * Created by IT SCHOOL on 09.11.2016.
 */
public class Dog extends Animal {

    String name;

    public String getName() {
        return name;
    }

    private int imageId = R.drawable.dog;

    public Dog(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }


    @Override
    public void eat() {
        Log.d("MyLog", "dog eat");
    }

    @Override
    public void sleep() {
        Log.d("MyLog", "dog sleep");
    }

    @Override
    public String toString(){
        return name;
    }
}
