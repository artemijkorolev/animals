package com.example.itschool.inheritanceexample.model;

import android.util.Log;

import com.example.itschool.inheritanceexample.R;
import com.example.itschool.inheritanceexample.model.Animal;

/**
 * Created by IT SCHOOL on 09.11.2016.
 */
public class Cat extends Animal {

    public String getName() {
        return name;
    }

    private String name;
    private int imageId = R.drawable.cat;

    public Cat (String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    @Override
    public void eat() {
        Log.d("MyLog", "cat eat");
    }

    @Override
    public void sleep() {
        Log.d("MyLog", "cat sleep");
    }
}
