package com.example.itschool.inheritanceexample.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.itschool.inheritanceexample.model.Animal;
import com.example.itschool.inheritanceexample.model.Cat;
import com.example.itschool.inheritanceexample.model.Dog;
import com.example.itschool.inheritanceexample.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout layout;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        layout = (LinearLayout) findViewById(R.id.layout_list);

        ArrayList<Animal> animals = new ArrayList<Animal>();


        animals.add(new Dog("Rex"));
        animals.add(new Cat("Murzik"));
        animals.add(new Cat("Barsik"));
        animals.add(new Dog("Sharik"));
        animals.add(new Dog("Rex"));
        animals.add(new Cat("Murzik"));
        animals.add(new Cat("Barsik"));
        animals.add(new Dog("Sharik"));
        animals.add(new Dog("Rex"));
        animals.add(new Cat("Murzik"));
        animals.add(new Cat("Barsik"));
        animals.add(new Dog("Sharik"));
        animals.add(new Dog("Rex"));
        animals.add(new Cat("Murzik"));
        animals.add(new Cat("Barsik"));
        animals.add(new Dog("Sharik"));

        for (Animal animal : animals) {

            ImageView image = new ImageView(this);
            image.setLayoutParams(new ViewGroup.LayoutParams(720,720));
            image.setImageResource(animal.getImageId());

            TextView text = new TextView(this);
            text.setText(animal.getName());

            layout.addView(image);
            layout.addView(text);

        }


    }

    @Override
    public void onClick(View view) {
        Log.d("MainActivity", "click");

    }
}
