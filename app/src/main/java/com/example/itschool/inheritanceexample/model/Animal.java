package com.example.itschool.inheritanceexample.model;

/**
 * Created by IT SCHOOL on 09.11.2016.
 */
public abstract class Animal {

    private String mistary_varrible;

    public abstract void eat();
    public abstract void sleep();
    public abstract int getImageId();
    public abstract String getName();

}
